from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class Friend(models.Model):
    name = models.CharField(max_length=30)
    url = models.URLField()
    
