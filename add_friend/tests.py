from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_friend
from .models import Friend
from .forms import Add_Friend_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class AddFriendUnitTest(TestCase):

	def test_friend_url_exist(self):
		response = Client().get('/friend/')
		self.assertEqual(response.status_code, 200)

	def test_friend_using_index_func(self):
		found = resolve('/friend/')
		self.assertEqual(found.func, index)

	def test_model_can_create_add_friend(self):
		# Creating a new activity
		new_activity = Friend.objects.create(name="Ham Pam", url="http://www.hampam.com")

		# Retrieving all available activity
		counting_all_available_friend = Friend.objects.all().count()
		self.assertEqual(counting_all_available_friend, 1)


	def test_form_todo_input_has_placeholder_and_css_classes(self):
		form = Add_Friend_Form()
		self.assertIn('class="form-control', form.as_p())
		self.assertIn('id="id_name"', form.as_p())
		self.assertIn('class="form-control', form.as_p())
		self.assertIn('id="id_url', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = Add_Friend_Form(data={'name': '', 'url': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['name'],
			["This field is required."]
		)

	def test_add_friend_success_and_render_the_result(self):
		response_post = Client().post('/friend/add_friend', {'name':'piscok','url':'p1scok.herokuapp.com'})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/friend/')
		html_response = response.content.decode('utf8')
		self.assertIn('piscok', html_response)
		
	def test_add_friend_error_and_render_the_result(self):
		response_post = Client().post('/friend/add_friend', {'name':'','url':''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/friend/')
		html_response = response.content.decode('utf8')
		self.assertNotIn('piscok', html_response)
		
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		
		super(AddFriendUnitTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(AddFriendUnitTest, self).tearDown()

	




