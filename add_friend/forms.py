from django import forms

class Add_Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'tolong isi bentuk url',
    }
    name_attrs = {
        'type': 'text',
        'class': 'form-control ',
        'placeholder':'Masukan name...'
    }
    url_attrs = {
        'type': 'url',
        'class': 'form-control',
        'placeholder':'Masukan url...'
    }
    field = ('nama','url')
    name = forms.CharField(label='Nama', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    url = forms.URLField(label='Url', required=True, widget=forms.URLInput(attrs=url_attrs))
