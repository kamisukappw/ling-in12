from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import Add_Friend_Form
from .models import Friend


# Create your views here.

response = dict()
response['total_friend'] = Friend.objects.count()

def index(request):
    response['content'] = index
    response['author'] = "kelompok 12"
    friend = Friend.objects.all()
    response['friend'] = friend
    response['add_friend_form'] = Add_Friend_Form
    
    html = 'add_friend.html' # html apps lu
    return render(request, html, response)

def add_friend(request):
    form = Add_Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
       
        add = Friend(name=response['name'],url=response['url'])
        add.save()
        return HttpResponseRedirect('/friend/')
    else:
        return HttpResponseRedirect('/friend/')

