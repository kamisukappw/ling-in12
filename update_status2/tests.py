from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, update_status, remove_status
from .models import Update
from .forms import Update_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class UpdateStatusUnitTest(TestCase):

	def test_update_status_url_is_exist(self):
		response = Client().get('/update/')
		self.assertEqual(response.status_code, 200)
		
	def test_root_url_now_is_using_index_page_from_update_status(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 301)
		self.assertRedirects(response,'/update/',301,200)

	def test_status_using_index_func(self):
		found = resolve('/update/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_status(self):
		# Creating a new status
		new_status = Update.objects.create(status='Hello happy world!')

		# Retrieving all available status
		counting_all_available_status = Update.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

	def test_form_todo_input_has_placeholder_and_css_classes(self):
		form = Update_Form()
		self.assertIn('class="todo-form-textarea', form.as_p())
		self.assertIn('id="id_status', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = Update_Form(data={'status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['status'],
			["This field is required."]
		)
		
	def test_status_post_success_and_render_the_result(self):
		response_post = Client().post('/update/update_status', {'status':'a'})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/update/')
		html_response = response.content.decode('utf8')
		self.assertIn('a', html_response)
		
	def test_status_post_error_and_render_the_result(self):
		response_post = Client().post('/update/update_status', {'title': '', 'description': ''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/update/')
		html_response = response.content.decode('utf8')
		self.assertNotIn('Piscok', html_response)
		
	def test_status_delete_post(self):
		new_status = Update.objects.create(status="Delete this")
		object = Update.objects.all()[0]
		response_post = Client().post('/update/remove_status', {'flag': str(object.id)})

		response= Client().get('/update/')
		html_response = response.content.decode('utf8')
		self.assertNotIn('<form id="form-' + str(object.id) + '"', html_response)
		
	def test_status_error_flag(self):
		new_status = Update.objects.create(status="Delete this")
		object = Update.objects.all()[0]
		response_post = Client().post('/update/remove_status', {'flag': "failed"})

		response= Client().get('/update/')
		html_response = response.content.decode('utf8')
		self.assertIn('<form id="form-' + str(object.id) + '"', html_response)
		