from django import forms

class Update_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    status_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'What\'s new?'
    }

    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=status_attrs))
