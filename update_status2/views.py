from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Update_Form
from .models import Update
from HalProfile.views import response as responseDelfa

# Create your views here.
response = dict()
response['total_feed'] = Update.objects.count()
response['last_update'] = Update.objects.last()
response.update(responseDelfa)

def index(request):
	update = Update.objects.all().order_by('-created_date')
	
	
	response['update'] = update
	html = 'update_status_new.html'
	response['update_form'] = Update_Form
	
	return render(request, html, response)
	
def update_status(request):
	form = Update_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		update = Update(status=response['status'])
		update.save()
		return HttpResponseRedirect('/update/')
	else:
		return HttpResponseRedirect('/update/')
		
def remove_status(request):
    try:
        idStatus = request.POST['flag']
        Update.objects.filter(id=idStatus).delete()
    except ValueError or KeyError:
        pass
    return HttpResponseRedirect('/update/')
