from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
# Create your tests here.

class statisticUnitTest(TestCase):
    def test_statistic_url_is_exist(self):
      response = Client().get('/statistic/')
      self.assertEqual(response.status_code, 200)

    def test_statistic_using_index_func(self):
      found = resolve('/statistic/')
      self.assertEqual(found.func, index)

    def test_statistic_using_template(self):
      response = Client().get('/statistic/')
      self.assertTemplateUsed(response, 'statistic.html')

    
    