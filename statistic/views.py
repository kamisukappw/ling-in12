from django.shortcuts import render
from add_friend.views import response
from update_status2.views import response as responseDanin
from HalProfile.views import response as responseDelfa


responseStat = dict()
responseStat.update(response)
responseStat.update(responseDanin)
responseStat.update(responseDelfa)
def index(request):
    return render(request,'statistic.html',responseStat)