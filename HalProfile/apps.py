from django.apps import AppConfig


class HalprofileConfig(AppConfig):
    name = 'HalProfile'
