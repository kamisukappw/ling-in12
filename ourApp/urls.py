"""ourApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView
import add_friend.urls as add_friend
import HalProfile.urls as HalProfile
import update_status2.urls as update_status2
import statistic.urls as statistic
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^friend/', include(add_friend, namespace='friend')),
    url(r'^HalProfile/', include(HalProfile, namespace='HalProfile')),
	url(r'^update/', include(update_status2, namespace='update')),
    url(r'^statistic/', include(statistic, namespace='statistic')),
	url(r'^$',RedirectView.as_view(url='/update/', permanent='true'),name='index'),
]
